package hockey

import (
	"fmt"
)

type Goal struct {
	time int
	home bool
}

func NewGoal(period int, minute int, second int, home bool) *Goal {
	time := (period-1)*(20*60) + minute*60 + second
	return &Goal{time: time, home: home}
}

func (g *Goal) Before(period int, minute int, second int) bool {
	time := (period-1)*(20*60) + minute*60 + second // Convert period/minute/second to seconds elapsed
	if g.time < time {
		return true
	} else {
		return false
	}

}

type Game struct {
	nAwayGoals int
	nHomeGoals int
	homeCode   string
	awayCode   string
	goals      []*Goal
}

func NewGame(homeCode string, awayCode string) *Game {
	return &Game{homeCode: homeCode, awayCode: awayCode, nAwayGoals: 0, nHomeGoals: 0, goals: []*Goal{}}
}

func (g *Game) AddGoal(period int, minute int, second int, teamCode string) {
	var home bool
	if teamCode == g.homeCode {
		home = true
	} else if teamCode == g.awayCode {
		home = false
	} else {
		g.Print()
		fmt.Println(teamCode, "not found.")
		panic("Error. Team code not found in game.")
	}
	nGoal := NewGoal(period, minute, second, home)
	if nGoal.home {
		g.nHomeGoals++
	} else {
		g.nAwayGoals++
	}
	g.goals = append(g.goals, nGoal)
}

func (g *Game) Print() {
	// Prints goal totals and each individual goal.
	fmt.Printf("Away: %v\n", g.nAwayGoals)
	fmt.Printf("Home: %v\n", g.nHomeGoals)
	for _, goal := range g.goals {
		fmt.Println(goal.time, ":", goal.home)
	}
}

func (g *Game) Winner() bool {
	if g.nHomeGoals > g.nAwayGoals {
		return true
	} else {
		return false
	}
	// 	} else if g.nAwayGoals > g.nHomeGoals {
	// 		return false
	// 	} else {
	// 		g.Print()
	// 		panic("Equal goals found")
	// 	}
}

func (g *Game) DifferentialAt(period int, minute int, second int) int {
	// Returns home-away goals at a given point in time
	nHomeGoals := 0
	nAwayGoals := 0
	for _, goal := range g.goals {
		if goal.Before(period, minute, second) {
			if goal.home {
				nHomeGoals++
			} else {
				nAwayGoals++
			}
		}
	}
	return nHomeGoals - nAwayGoals
}

package main

import (
	// "bufio"
	"fmt"
	// "strings"
	// "encoding/csv"
	// "io"
	// "os"

	"bitbucket.org/cpalone/hockey"
)

// func parseLine(line string) int, *hockey.Goal {
// 	fields := strings.Split(line, "|")
// 	id := fields[0]
// 	rawTime := strings.split(fields[3], ":")
// 	minute := rawTime[0]
// 	second := rawTime[1]
// 	period := fields[2]
// 	g := hockey.NewGoal()
// }

func main() {
	// SAMPLE_GAME := "20130001|Montreal Canadiens|3|Toronto Maple Leafs|4|Tuesday, October 1, 2013| 7:27 EDT; Fin/End 10:05 EDT</td>| Centre Bell| 21,273 ||||"
	// teamCodes := hockey.LoadTeamCodes("../data/team_code.csv")
	// teamCode1 := "Montreal Canadiens"
	// teamCode2 := "Toronto Maple Leafs"
	// fmt.Println(teamCodes[teamCode1])
	// fmt.Println(teamCodes[teamCode2])
	// game := hockey.NewGame(teamCodes[teamCode1], teamCodes[teamCode2])
	// fmt.Println(game)

	// games := hockey.LoadGames("../data/2013-game.dat")
	games := hockey.LoadMultipleGameFiles([]string{"../data/2013-game.dat", "../data/2012-game.dat", "../data/2011-game.dat", "../data/2010-game.dat", "../data/2009-game.dat", "../data/2008-game.dat"})
	hockey.AddGoalFile("../data/2013-goal.dat", games)
	hockey.AddGoalFile("../data/2011-goal.dat", games)
	hockey.AddGoalFile("../data/2010-goal.dat", games)
	hockey.AddGoalFile("../data/2009-goal.dat", games)
	hockey.AddGoalFile("../data/2008-goal.dat", games)
	// hockey.LoadGoals("../data/2013-goal.dat", games)
	fmt.Println("Games and goals loaded.")
	// fmt.Println(games["20130001"].)
	// games["20130001"].Print()
	// fmt.Println(games["20130001"].DifferentialAt(1, 0, 0))
	// fmt.Println(games["20130001"].DifferentialAt(2, 0, 0))
	// fmt.Println(games["20130001"].DifferentialAt(3, 0, 0))
	periods := []int{1, 2, 3}
	minutes := []int{4, 8, 12, 16, 20}
	for _, period := range periods {
		for _, minute := range minutes {
			n := 0
			w := 0
			for _, game := range games {
				if game.DifferentialAt(period, minute, 0) == 0 {
					n++
					if game.Winner() {
						w++
					}
				}
			}
			fmt.Println(period, minute, "0", w, n)
		}
	}
}

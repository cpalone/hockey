package hockey

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func ReadFile(path string) []string {
	f, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	lines := []string{}
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines

}

func ReadCSV(path string, sep string) [][]string {
	lines := ReadFile(path)
	fields := make([][]string, len(lines))
	for i, line := range lines {
		fields[i] = strings.Split(line, sep)
	}
	return fields
}

func LoadTeamCodes(path string) map[string]string {
	teamCodes := make(map[string]string)
	fields := ReadCSV(path, "|")
	for _, line := range fields {
		teamCodes[line[0]] = line[1]
		teamCodes[line[1]] = line[0]
	}
	return teamCodes
}

func addGameFile(path string, teamCodes map[string]string, games map[string]*Game) {
	gameFields := ReadCSV(path, "|")
	for _, lineField := range gameFields {
		homeCode, ok := teamCodes[lineField[1]]
		if !ok {
			fmt.Println(lineField[1], path)
			panic("Could not convert name to code!")
		}
		awayCode, ok := teamCodes[lineField[3]]
		if !ok {
			fmt.Println(lineField[3], path)
			panic("Could not convert name to code!")
		}
		id := lineField[0]
		game := NewGame(homeCode, awayCode)
		games[id] = game
	}
}

func LoadMultipleGameFiles(paths []string) map[string]*Game {
	teamCodes := LoadTeamCodes("../data/team_code.csv")
	games := make(map[string]*Game)
	for _, path := range paths {
		addGameFile(path, teamCodes, games)
	}
	return games
}

func AddGoalFile(path string, games map[string]*Game) {
	goalFields := ReadCSV(path, "|")
	for _, lineField := range goalFields {
		id := lineField[0]
		rawPeriod := lineField[2]
		period, err := strconv.Atoi(rawPeriod)
		if err != nil {
			fmt.Println(lineField)
			panic(err)
		}
		rawTime := strings.Split(lineField[3], ":")
		minute, err := strconv.Atoi(rawTime[0])
		if err != nil {
			panic(err)
		}
		second, err := strconv.Atoi(rawTime[1])
		if err != nil {
			panic(err)
		}
		teamCode := lineField[4]
		procGame := games[id]
		procGame.AddGoal(period, minute, second, teamCode)
		// fmt.Println(procGame)
	}
}
